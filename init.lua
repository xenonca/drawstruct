local function place_struc(pos, node)
	for i = 7, 1, -1 do
		new_pos = {x=pos.x + math.random(-3,3), y=pos.y + i, z=pos.z + math.random(-3,3)}
			minetest.add_particlespawner({
				amount = 20,
				time = 0.1,
				minpos = {x = new_pos.x + 1, y = new_pos.y + 1, z = new_pos.z + 1},
				maxpos = {x = new_pos.x - 1, y =new_pos.y - 1, z = new_pos.z - 1},
				minvel = {x = -1, y = -1, z = -1},
				maxvel = {x = 1, y = 1, z = 1},
				minacc = {x = 3, y = 3, z = 3},
				maxacc = {x = -3, y = -3, z = -3},
				minexptime = 1,
				maxexptime = 1.5,
				minsize = 0.3,
				maxsize = 1,
				texture = "ptcls.png",
				collisiondetection = false,
			})
			minetest.set_node(new_pos, node)
	end
end

minetest.register_tool("drawstruct:ds_tool", {
	description = "Drawstructor",
	inventory_image = "default_tool_mesesword.png^default_stick.png",
	liquids_pointable = true,
	range = 10,
	on_place = function(itemstack, placer, pointed_thing)
		local pos = minetest.get_pointed_thing_position(pointed_thing)
		local node = minetest.get_node(pos)
		local new_pos = {x=pos.x, y=pos.y + 1, z=pos.z}
		minetest.set_node(new_pos, node)
	end,
	on_use = function(itemstack, user, pointed_thing)
		local pos = minetest.get_pointed_thing_position(pointed_thing)
		if pos ~= nil then
			local node = minetest.get_node(pos)
			place_struc(pos, node)
		end
		return nil
	end,
})