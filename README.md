# Drawstruct
Submission for an event on [MTD](https://discord.gg/minetest).
***
![screenshot.png](screenshot.png)
## Description
Right-click while holding the Drawstructor to rise walls, pillar up or draw shapes on the floor. Alternatively left-click to spawn random parkours. Compete with others by climbing up your parkour and at the same time generating your next steps by left-clicking the node you're standing on.

## Dependencies
default ([MTG](https://content.minetest.net/packages/Minetest/minetest_game/))

## Licence
Code: MIT by xenonca  
Media: CC-BY-SA 4.0 by xenonca
